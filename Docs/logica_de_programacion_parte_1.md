## **Realice este curso para Lógica de Programación y:**

- De sus primeros pasos de manera práctica con el lenguaje JavaScript
- Comience en el fascinante mundo de programación usando solo su Navegador
- Entienda las variables y funciones y para qué sirven
- Cree programas y practique con problemas del día a día
- Coloque sus conocimientos en práctica automatizando actividades repetitivas

### **Aulas**

- **Comience a programar hoy**

  - Presentación
  - Preparando el ambiente
  - Herramienta de trabajo
  - Creando tu propio código HTML
  - El Lenguaje HTML
  - Esto sí es programación
  - Mundo HTML y JavaScript
  - Disciplina y Organización
  - Haz lo que hicimos en aula
  - Lo que aprendimos

- **Comuníquese con el usuario**

  - Proyecto del aula anterior
  - Convenio de codificación
  - Convención en el uso de etiquetas
  - Concatenando caracteres
  - ¿Cuál es el resultado?
  - Operaciones con textos y números
  - Ejemplos de concatenaciones
  - Comprender aún más lo que sucede
  - Haz lo que hicimos en aula
  - Lo que aprendimos

- **Deje su programa dinámico usando Variables**

  - Proyecto del aula anterior
  - Reduciendo alteraciones
  - Corrigiendo errores de otros
  - Variables
  - ¿Dónde está el error?
  - ¡Facilitando nuestras vidas!
  - ¿Alcohol o gasolina?
  - Haz lo que hicimos en aula
  - Lo que aprendimos

- **Cree sus propias funcionalidades**

  - Proyecto del aula anterior
  - Mejorando el mantenimiento de código
  - Funciones
  - Sobre funciones
  - Funciones con parámetros
  - ¡Alerta diferente!
  - Cálculo diferencia de edades
  - Haz lo que hicimos en aula
  - Lo que aprendimos

- **Practique resolviendo problemas del dia a dia**

  - Proyecto del aula anterior
  - Calculando IMC
  - Cálculo del IMC, aplicación
  - Explorando a fondo el retorno de funciones
  - Función que recibe más de un parámetro
  - ¿Cuál es el resultado?
  - Interactuando con el usuario
  - Leyendo desde el teclado
  - Haz lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
