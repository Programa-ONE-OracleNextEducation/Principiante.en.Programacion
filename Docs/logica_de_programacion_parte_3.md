## **Realice este curso para Lógica de Programación y:**

- Continue praticando lógica com JavaScript
- Use y abuse de funciones para facilitar su trabajo
- Conozca el Canvas para generar diagramas y animaciones
- Revise conceptos importantes de programación
- Cree su primer juego

### **Aulas**

- **Diseñando gráficos con Canvas**

  - Presentación
  - Preparando el ambiente
  - Canvas será nuestra pantalla
  - Diseñando una bandera con un pincel
  - Siendo creativos con la bandera
  - Nuestro pincel y sus características
  - La cara de Creeper
  - Diseñando una Escuadra
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Utilizando funciones**

  - Proyecto del aula anterior
  - Repetir código no es de buenos programadores
  - Una función más genérica
  - Altura aleatoria del rectángulo
  - Las iteraciones son fantásticas
  - Diseñando una flor
  - Gráfico de Barras
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Interactuando con el usuario**

  - Proyecto del aula anterior
  - Nuestra pantalla tiene vida, responde sola
  - Asociando eventos con funciones
  - Mouse, en cuál posición estás
  - Una forma diferente de probar un if
  - Cambiando de color
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Moviendo elementos con animaciones simples**

  - Proyecto del aula anterior
  - Ya diseñamos un círculo, ahora a darle vida
  - Diseñando
  - Animaciones Simples
  - Ayudando a nuestra compañera con su código
  - ¡Ya que va, que vuelva!
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Nuestro primer juego**

  - Proyecto del aula anterior
  - Creando Objetivo Aleatorio
  - Disparando contra el objetivo
  - Redondeo de valores
  - Diseñando con el mouse
  - Haz lo que hicimos en el aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
