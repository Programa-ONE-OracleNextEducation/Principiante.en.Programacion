## **Realice este curso para HTML y CSS y:**

- Aprenda la estructura de la página HTML
- Navegue entre páginas Web
- Conozca el reset.css y el prosicionamiento por el CSS
- Entienda las diferencias entre inline-block
- Aprenda a lidiar con bordes y capturar eventos con CSS
- Cree encabezado, cuerpo y pié de página de una página Web

### **Aulas**

- **Creando una nueva página**

  - Presentación
  - Preparando el ambiente
  - Revisión
  - Nueva Página
  - Estructura de una página HTML
  - Encabezado
  - Logo del sitio web
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Navegación entre páginas**

  - Proyecto del aula anterior
  - Estructurando la navegación
  - Navegando entre las páginas
  - Ajustando la lista
  - Texto en Mayúscula
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Posicionamiento de los elementos**

  - Proyecto del aula anterior
  - Descarga el archivo reset.css
  - Limpiando el CSS
  - Cómo dejar el CSS limpio
  - Cómo funciona el posicionamiento
  - Posicionamiento
  - Posicionando el encabezado
  - Centralizando elementos
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Lista con Productos**

  - Proyecto del aula anterior
  - Divisiones semánticas
  - ¿Por qué usar las etiquetas semánticas?
  - Creando listas complejas
  - Ítems de la lista
  - Reforzando el inline-block
  - Comportamientos de bloques
  - Ajustando el tamaño de los elementos
  - Espaciamiento interno
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Lidiando con bordes**

  - Proyecto del aula anterior
  - Aplicando bordes
  - Funcionamiento de los bordes
  - Bordes redondeados
  - CSS para las esquinas redondeados
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Capturando eventos con CSS**

  - Proyecto del aula anterior
  - Capturando el movimiento del mouse
  - Comportamiento de los elementos
  - Capturando el clic del mouse
  - Comportamiento del mouse
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Finalizando la página de productos**

  - Proyecto del aula anterior
  - Pie de Página
  - Imagen de fondo
  - Caracteres Especiales
  - Tipos de caracteres especiales
  - Haz lo que hicimos en el aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
