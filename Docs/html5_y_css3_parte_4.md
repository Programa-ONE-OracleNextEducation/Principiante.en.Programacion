## **Realice este curso para HTML y CSS y:**

- Importe contenido externo a tu página HTML, como fuentes, videos y mapas
- Conozca las pseudoclases y los pseudoelementos
- Utilice selectores CSS avanzados
- Aprenda a lidiar con la opacidad y la sombra
- Comprenda Viewport y design responsivo

### **Aulas**

- **Adaptando la página inicial**

  - Presentación
  - Preparando el ambiente
  - Rehaciendo la página inicial
  - Revisando el contenido
  - Adaptando el CSS
  - Buenas prácticas de CSS
  - Descarga la imagen utensilios.jpg
  - Usando el float
  - Utilidad del Float
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Contenido externo**

  - Proyecto del aula anterior
  - Fuentes Externas
  - Beneficios de usar fuentes externas
  - Trabajando con mapas
  - La etiqueta iframe
  - Importando un video
  - Importando vídeos en nuestras páginas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Mejorando el CSS**

  - Proyecto del aula anterior
  - Ajustando los diferenciales
  - Pseudo-classes
  - Explorando las pseudo-clases
  - Aplicando gradientes
  - Degradado de 5 colores en un elemento
  - Pseudo-elementos
  - Creando un elemento antes que otro, con CSS
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Seleccionando cualquier cosa**

  - Proyecto del aula anterior
  - Selectores avanzados
  - Seleccionando un elemento específico
  - Cálculo con CSS
  - Medidas proporcionales
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Opacidad y sombra**

  - Proyecto del aula anterior
  - Opacidad en los elementos
  - ¿Quién puede recibir una capa de opacidad?
  - Sombra en los elementos
  - Sombra interna en un elemento
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Diseño responsivo**

  - Proyecto del aula anterior
  - Meta tag de Viewport
  - Presentar un elemento de otra manera con media query
  - Adaptar una página para celular
  - No perder el tiempo con layouts responsivos
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
