## **Realice este curso para Builds y Control de versiones y:**

- ¿Qué es Git y Github?
- Entiende un sistema de control de versión
- Guarda y recupera tu código en diferentes versiones
- Resuelve merges y conflictos
- Trabaja con diferentes branches

**Aulas**

- **Qué es Git?**

  - Presentación
  - Preparando el ambiente
  - Para qué sirve Git?
  - Utilidad de un VCS
  - Instalando Git
  - Para saber más: Instalación
  - Repositorios
  - Primeros pasos
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Iniciando los trabajos**

  - Proyecto del aula anterior
  - Guardando las modificaciones
  - Para saber más: ¿Quién eres tú?
  - Para saber más: git status
  - Viendo el histórico
  - Primera configuración de Git
  - Para saber más: git log
  - Ignorando archivos
  - Para saber más: Cuándo commitear
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Compartiendo el trabajo**

  - Proyecto del aula anterior
  - Repositorios remotos
  - Servidor Git
  - Trabajando con repositorios remotos
  - Sincronizando los datos
  - Compartimos las modificaciones
  - GitHub
  - Para saber más: GitHub
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Trabajando en equipo**

  - Proyecto del aula anterior
  - Branches
  - Para saber más: Ramificaciones
  - Uniendo el trabajo
  - Merge de branches
  - Actualizando la branch
  - Rebase vs Merge
  - Resolviendo conflictos
  - Para saber más: Conflictos con rebase
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Manipulando las versiones**

  - Proyecto del aula anterior
  - Ctrl + Z en Git
  - Deshaciendo el trabajo
  - Guardando para despues
  - Guardando temporalmente
  - Viajando en el tiempo
  - Checkout
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Generando entregas**

  - Proyecto del aula anterior
  - Viendo las modificaciones
  - Exhibición de las modificaciones con diff
  - Tags y Releases
  - Tags en GitHub
  - Haga lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
