## **Realice este curso para HTML y CSS y:**

- Aprenda lo que es HTML y CSS
- Entienda la estructura básica de un archivo HTML
- Utilice el navegador para inspeccionar elementos
- Aprenda a definir estilos para elementos usando CSS
- Desarrolle una página Web desde cero

### **Aulas**

- **Marcacion del primer texto**

  - Presentación
  - Preparando el ambiente
  - Definiendo texto
  - Uso de etiquetas
  - Mejorando el Texto
  - Estructura de énfasis correcta
  - Haz lo que hicimos en aula
  - Lo que aprendimos en esta aula

- **Separacion de Contenido y informaciones**

  - Proyecto del aula anterior
  - Estructura básica
  - Estructura del HTML
  - Pasando datos para el navegador
  - Lidiando con la acentuación
  - Separando contenido e información
  - Estructura del contenido
  - Haz lo que hicimos en aula
  - Lo que aprendimos en esta aula

- **Trabajando con css**

  - Proyecto del aula anterior
  - Comenzando con CSS
  - Propiedad y su valor en el CSS
  - Organizando el estilo
  - Estilo en cascada
  - Alterando el color
  - Extra: Colores hexadecimales
  - Representando colores en el CSS
  - Haz lo que hicimos en aula
  - Lo que aprendimos en esta aula

- **Estilizando imagenes**

  - Proyecto del aula anterior
  - Identificador de elemento y etiqueta de imágen
  - Adicionando una imagen
  - CSS para imágenes
  - Alterando la anchura de un elemento
  - Extra: Equipo de Front-End
  - Haz lo que hicimos en aula
  - Lo que aprendimos en esta aula

- **Lista y divisiones del contenido**

  - Proyecto del aula anterior
  - Trabajando con listas
  - Estructura de una lista en HTML
  - Divisiones de contenido
  - Creando bloques de contenido
  - Inline y Block
  - Uso de la propiedad class en nuestro código
  - Haz lo que hicimos en aula
  - Lo que aprendimos en esta aula

- **Finalizando la página**

  - Proyecto del aula anterior
  - Encabezado
  - Etiqueta de Encabezado
  - Haz lo que hicimos en aula
  - Proyecto Final
  - Lo que aprendimos en esta aula
  - Conclusión
