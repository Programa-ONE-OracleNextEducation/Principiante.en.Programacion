## **Realice este curso para Lógica de Programación y:**

- Perfeccione sus conocimientos en lógica de programación usando JavaScript
- Use condiciones dentro de sus programas
- Aprenda a realizar iteraciones e loops con ciclos for e while
- Haga que sus programas interactúen con el usuario
- Entienda el concepto de arrays, cuando y como usarlos

### **Aulas**

- **Ejecute códigos diferentes dependiendo de la condición**

  - Presentación
  - Preparando el ambiente
  - Convirtiendo texto en numero
  - Total de invitados de la fiesta
  - Trabajando con condiciones
  - ¿Será que entra cumpliendo las condiciones?
  - Mejorando el programa de IMC
  - Juego de Adivinación
  - La condición if y else
  - ¿Será qué puedo conducir?
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Repita Actividades**

  - Proyecto del aula anterior
  - Mientras que
  - Dejando muy claro que erró
  - Otra forma de Repetir
  - De while para for
  - Acumulando Variables
  - Todos los números pares del 1 al 100
  - Interrumpiendo una Repetición
  - Interrumpiendo una Repetición
  - El programa que no para de preguntar
  - Repeticiones Anidadas
  - Simulando una pantalla de inicio de sesión
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Interactúe de manera diferente con el usuario**

  - Proyecto del aula anterior
  - Campo de Texto y Botón
  - Más interacción del mundo JavaScript con mundo HTML
  - Mejorando la usabilidad
  - Exhibiendo en un alert el nombre digitado
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Trabaje con muchos datos**

  - Proyecto del aula anterior
  - Almacenando muchos datos
  - Declarando un array
  - Usando Loop para carga de datos
  - Tamaño del Array
  - El tamaño de un array
  - Superman quedó fuera por un pelo
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Aprovechando las ventajas de los arrays**

  - Proyecto del aula anterior
  - Manipulando Array
  - Ensalada de fruta
  - Validando duplicados
  - Refinando nuestro código
  - ¡No hay ingredientes repetidos aquí!
  - Haz lo que hicimos en el aula
  - Proyecto Final
  - Lo que aprendimos
  - Conclusión
