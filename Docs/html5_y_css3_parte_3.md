## **Realice este curso para HTML y CSS y:**

- Cree formularios complejos
- Trabaje con campos para dispositivos móviles
- Presente informacioes en tablas
- Use estilos para formularios, campos y tablas
- Entienda la jerarquía en el CSS
- Trabaje con transformaciones y transiciones

### **Aulas**

- **Creando una nueva página**

  - Presentación
  - Preparando el ambiente
  - Comenzando la página de contacto
  - Funcionamiento de Formularios
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Comenzando un formulario**

  - Proyecto del aula anterior
  - Campos básicos
  - Estructura básica
  - Estilos para formulario
  - Input con espacio entre el borde y el contenido
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Tipos de campos diferentes**

  - Proyecto del aula anterior
  - Formulario más completo
  - Campos complejos
  - CSS para inputs más complejos
  - CSS repetido
  - Jerarquía en el CSS
  - Color final del párrafo
  - Seleccionando opciones
  - Selects y options
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Mejorando la semántica**

  - Proyecto del aula anterior
  - Inputs para celulares
  - Celulares y Formularios
  - Datos importantes en los inputs
  - Formularios más fáciles rellenar
  - Mejorando la semántica del formulario
  - Título de un grupo de campos del formulario
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **CSS avanzado**

  - Proyecto del aula anterior
  - Qué son las transiciones
  - Transición con duración
  - Entendiendo Transformaciones
  - Aumentando proporcionalmente
  - Haz lo que hicimos en el aula
  - Lo que aprendimos

- **Estructura de tablas**

  - Proyecto del aula anterior
  - Tabla básica
  - Creando filas en una tabla
  - Etiquetas semánticas para tablas
  - Cómo dividir una tabla
  - Estilos en las tablas
  - Haz lo que hicimos en el aula
  - Proyecto final
  - Lo que aprendimos
  - Conclusión
