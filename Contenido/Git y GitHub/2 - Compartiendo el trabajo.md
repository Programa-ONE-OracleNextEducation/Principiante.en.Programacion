# **Compartiendo el trabajo**

## **Repositorios remotos (localmente)**

- Carpetas:

  - mi-app # Donde trabajamos el proyecto.
  - other-user # Donde trabajamos el proyecto con otro usuario.
  - servidor # Donde subiremos (localmente) nuestro trabajo.

- Dentro de la carpeta **servidor**
  ```sh
  git init --bare
  ```
  Retorna el camino de nuestro repositorio remoto.
  Con este comando creamos un repositorio que no tendrá la working tree, o sea, no contendrá una copia de nuestros archivos. Como el repositorio servirá solo como servidor, para que otros miembros del equipo sincronicen sus trabajos, disminuimos espacio de almacenamiento de esta forma.
- Retorna la lista de los repositorios remotos
  ```sh
  git remote
  ```
- Agrega un repositorio remoto
  ```sh
  git remote add <name_repo> <url_repo>
  ```
- Para ver la url de los repos. **fetch** el de donde nos va a trae los cambios y **push** a donde los va a subir.
  ```sh
  git remote -v
  ```
- Para clonar un repositorio remoto desde la carpeta **other-user**.
  ```sh
  git clone /C/Users/Javier/Desktop/servidor/ <name_project>
  ```

![Creacion de repositorio remoto](img/creacion_de_repositorio_remoto.png "Creacion de repositorio remoto")

## **Sincronizando datos**

- Dentro de la carpeta **mi-app**, subimos los cambios al **servidor**.
  ```sh
  git push <name_servidor> <name_branch>
  ```
- Dentro de la carpeta **other-user**, Para cambiar el nombre del **servidor** (default origin)
  ```sh
  git remote rename origin servidorLocal
  ```
- Para traernos los nuevos cambios remotos.
  ```sh
  git pull <name_repo_remote> <name_branch_remote>
  ```

![Sincronizando datos](img/sincronizando_datos.png "Sincronizando datos")

## **Como trabajar con GitHub**

**origin** es el repositorio principal por defecto, por convencion se eligio ese nombre.

![Creacion de repo en GitHub](img/creacion_de_repo_en_github.png "Creacion de repo en GitHub")
