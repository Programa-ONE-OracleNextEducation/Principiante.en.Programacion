# **Iniciando los trabajos**

## **Guardando las modificaciones**

- Para que Git vea una carpeta en particular como un repositorio y observe los cambios en sus archivos (inicializa un repositorio).
  ```sh
  git init
  ```
- Para saber los cambios realizados y el estado en el que estan nuestros archivos dentro de nuestro repositorio.
  ```sh
  git status
  ```
- Para trackear los archivos por git. No estan siendo monitoreados, entonces para guardar una modificacion de un archivo en git.
  ```sh
  git add <name_file>
  ```
- Para guardar esa modificacion en el repositorio de git.
  ```sh
  git commit -m "<message>"
  ```

![Guardando modificaciones](img/guardando_las_modificaciones.png "GM")

- Para configurar git remotamente. La misma configuracion para todos los proyectos del computador.
  ```sh
  git config --global user.email "<email>"
  git config --global user.name "<name>"
  ```
- Para configurar git localmente. Las configuraciones solo se haran para este repositorio.
  ```sh
  git config --local user.email "<email>"
  git config --local user.name "<name>"
  ```
- Para el historico de modificaciones, cada commit que se hizo en el proyecto.
  ```sh
  git log
  ```
  Para salir tipeamos :q y Enter.
- Para ver el historico en una linea.
  ```sh
  git log oneline
  ```
- Para ver el historico y las modificaciones de los archivos.
  ```sh
  git log -p
  ```
  [Mas info sobre log](https://devhints.io/git-log "git log").
- Ejemplo de git log personalizado.
  ```sh
  git log --pretty="format:%h %s %ae"
  ```
- Para ignorar archivos o carpetas que no queremos monitoriar con git, debemos crearun archivo llamado **.gitignore**. Dentro de el:
  ```sh
  <name_carpeta>/     #ignora la carpeta y su contenido.
  <name_archivo>      #ignora un archivo
  ```

## **Para saber más: ¿Quién eres tú?**

Antes de cualquier interacción con git, es necesario informar quién eres para que almacene correctamente los datos del autor de cada uno de los cambios en el código.

En el video no hice mucho hincapié en esto, pero para que puedas hacer esto en tu computadora, si estás comenzando a usar git ahora, simplemente escribe los siguientes comandos (estando en la carpeta del repositorio de git):

```sh
git config --local user.name "Tu nombre aquí"
git config --local user.email "Tu@email.aqui”
```

## **Para saber más: git status**

Al ejecutar el comando git status, recibimos información que puede no ser tan clara, especialmente cuando nos encontramos con términos como HEAD, working tree, index, etc.

Solo para aclarar un poco, ya que entenderemos mejor cómo funciona Git durante el curso, aquí hay algunas definiciones interesantes:

- **HEAD**: Estado actual de nuestro código, es decir, donde nos colocó Git
- **Working tree**: Lugar donde los archivos realmente están siendo almacenados
- **index:** Lugar donde Git almacena lo que será commiteado, es decir, la ubicación entre el working tree y el repositorio de Git en sí.

Además de eso, los posibles estados de nuestros archivos son explicados con detalle en este [link](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio "Fundamentos de Git : Guardando cambios en el repositorio").

## **Para saber más: git log**

Como vimos en el último video, podemos ver el historial de cambios de nuestro proyecto de manera muy sencilla, usando el comando git log.

Aunque es fácil, este comando es muy poderoso. Ejecuta git log --help y ve algunas de las posibles opciones. Para obtener algunos ejemplos más fáciles de entender, puedes buscar sobre git log o echar un vistazo a este [link](https://devhints.io/git-log "git log").

## **Para saber más: Cuándo commitear**

Deberíamos generar un commit siempre que nuestra base de código esté en un estado que nos gustaría recordar. Nunca deberíamos tener commits de código que no funcionen, pero tampoco es interesante dejar el commit solo al final de una función.

Esta puede ser una discusión interminable y cada empresa o equipo puede seguir una estrategia diferente. ¡Estudien al respecto, comprendan qué tiene más sentido para ustedes y su equipo y sean felices!
