# **Manipulando las versiones**

## **Ctrl + z en Git**

- Para deshacer el git add <name_file>. Es como si nunca lo hubieramos hecho.
  ```sh
  git restore --staged <name_file>
  ```
- Para deshacer un git commit <name_file>. Hacemos un git log, copiamos el hash, del commit a deshacer y hacemos.
  ```sh
  git revert <hash_del_commit>
  ```

## **Guardando temporalmente**

- Si no queremos commitear algo podemos guardarlo temporalmente
  ```sh
  git stash
  ```
- Para listar los guardados temporales
  ```sh
  git stash list
  ```
- Para traer ese cambio no commiteado
  ```sh
  git stash pop
  ```

Vimos cómo podemos utilizar git stash para almacenar temporalmente algunas de nuestras modificaciones.
¿En qué momento el stash es útil? Rta.: Cuando necesitamos pausar el desarrollo de alguna funcionalidad, o corrección, antes de finalizar, y tal vez no sea interesante realizar un commit, pues nuestro código puede no estar funcionando aún. En este caso es interesante guardar el trabajo para poder volver a él después.

## **Viajando entre commits**

- Para movernos entre commits. Con los primeros 7 caracteres del hash es suficiente.
  ```sh
  git checkout <hash_del_commit>
  ```
  Cuando nos paramos en un commit, en principio estamos desanexado de nuestro estado head, podemos ver, hacer cambios, commitear y esos cambios se descartan cuando volvamos a nuestro branch. Para que eso no pase, debemos crear otra branch y ahora si tenemos una linea de trabajo en la cual desarrollar, commitear y subir.

![Viajando entre commits](img/viajando_entre_commits.png "Viajando entre commits")

## **Checkout**

Ya utilizamos en más de una ocasión el comando git checkout. Resumidamente, ¿para qué sirve el comando git checkout?

La descripción del comando git checkout --help, en una traducción libre es: “Actualizar los archivos en working tree para que queden en la versión especificada. […]”.
Básicamente, podemos dejar nuestro código en el estado de último commit de una branch, de un commit específico, o incluso tags (que veremos más adelante).
