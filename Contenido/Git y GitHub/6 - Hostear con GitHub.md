# **Hostear con GitHub**

## **Pasos**

1. Crear el repositorio.
2. Subir la pagina en el branch por defecto (master o main).
3. Ir a Settings > Actions > Pages y seleccionar el branch que tiene nuestra app > save.

![GitHub Pages 1](img/github_pages_1.png "GitHub Pages 1")

4. Finalmente podemos ver la url de nuestra app.

![GitHub Pages 2](img/github_pages_2.png "GitHub Pages 2")
