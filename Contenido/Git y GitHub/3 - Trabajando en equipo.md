# **Trabajando en equipo**

## **Branches**

- Para listar las ramas (branch)
  ```sh
  git branch
  ```
- Para crear un branch
  ```sh
  git branch <name_branch>
  ```
- Para movernos a un branch
  ```sh
  git checkout <name_branch>
  ```
- Para crear y movernos a un branch
  ```sh
  git checkout -b <name_branch>
  ```

![Trabajando con branch](img/trabajando_con_branch.png "Trabajando con branch")

[Simulador de como trabajar con branch](https://git-school.github.io/visualizing-git/ "Simulador de como trabajar con branch").

## **Uniendo el trabajo**

- Combinar los cambios de <name_other_branch> con <master> (local). Nos paramos o movemos al branch master y hacemos:
  ```sh
  git merge <name_other_branch>
  ```

Por defecto el commit tiene el nombre de **Merge branch <name_other_branch>**, el cual se lo podemos cambiar.

![Merge branch](img/merge_branch.png "Merge branch")

Para salir tipeamos :x y Enter.

- Para traer los commit de <name_other_branch> a <master> sin crear un commit y manteniendo el ultimo commit de master como trabajo realizado. Los commit traidos se colocan detras del ultimo commit de master.
  ```sh
  git rebase <name_other_branch>
  ```

## **Para saber más: Ramificaciones**

Las branches ("ramas") se utilizan para desarrollar funcionalidades aisladas entre sí. La branch master es la branch "predeterminada" cuando creas un repositorio.

Es interesante separar el desarrollo de funcionalidades en diferentes branches, para que los cambios en el código de una no influyan en el funcionamiento de otra.

## **Rebase vs Merge**

Ya sabemos cómo traer el trabajo de otra Branch y unir con la Branch actual. Conocemos dos formas de hacer esto: **merge** y **rebase**.

En este escenario, ¿cuál es la diferencia entre los comandos **rebase** y **merge**?

Con esto evitamos los commits de merge. Hay una larga discusión sobre lo que es “mejor”: rebase o merge; Estudie, busque, y genere sus propias conclusiones. Acá tienen un [artículo](https://medium.com/datadriveninvestor/git-rebase-vs-merge-cc5199edd77c "rebase vs merge") (de miles de otros) que hablan sobre el asunto .
