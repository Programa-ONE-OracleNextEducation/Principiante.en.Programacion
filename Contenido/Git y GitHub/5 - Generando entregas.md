# **Generando entregas**

## **Ctrl + z en Git**

- Para las modificaiones en codigo desde un commit hasta otro.
  ```sh
  git diff <hash_commit_since>..<hash_commit_until>
  ```
- Para las modificaiones en codigo sin que este agregado (git add .).
  ```sh
  git diff
  ```

## **Tags y Releases**

- Para marcar que hay una version de nuestro proyecto. -a indica que estoy agregado un tag. Por ejemplo lo podemos llamar "v0.1.0". -m indica que queremos escribir una descripcion.
  ```sh
  git tag -a <name_tag> -m "<message>"
  ```
- Para listar los tags.
  ```sh
  git tag
  ```
- Para subir el tag.
  ```sh
  git push origin <name_tag>
  ```

¿Qué resultado genera el envío de una tag para Github? Rta.: Github nos da la posibilidad de bajar un archivo compactado que contiene el código en el estado en que el tag fue generado.
