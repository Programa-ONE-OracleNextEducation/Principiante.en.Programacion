# **Principiante en Programacion**

## **¡Conoce esta formación!**

Si no conoces absolutamente nada sobre programación y desarrollo de software, pero desear ingresar al área desarrollo y de tecnología, entonces esta formación de Principiante en Programación es para tí.

Recuerda que el mercado global tiene una demanda muy grande por profesionales de tecnología y principalmente programadores, y lo más impresionantemente es que cada año esa demanda crece aún más.

En esta formación lo primero que aprenderás será sobre Lógica de Programación, que es la base de cualquier carrera o profesión relacionada con tecnología.

Si trabajas en otras áreas como ciencia de datos, marketing digital, experiencia de usuario (UX), diseño u otras relacionadas, esta formación te ayudará a entender mejor como el área de los desarrolladores funciona. De esta forma podrás comunicarte de manera más fluida y objetiva con el equipo de Desarrollo.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Christian Velasco
- Bruno Darío Fernández Ellerbach

## **Paso a paso**

### **1. Plan de estudios**

El plan de estudio es el primer paso para ayudarte a organizar tu rutina de estudio.

Fue creado estratégicamente para guiarte en este nuevo camino de aprendizaje, ayudarte a estudiar en menos tiempo y aumentar tu rendimiento.

En este plan de estudios encontrarás el orden sugerido en el que debe hacer los cursos y el tiempo estimado en el que debe completar cada módulo. Con él podrás seguir el ritmo del programa ONE, aprovechar al máximo tus tiempos de estudio y cada ruta de aprendizaje.

El plan de estudio se realizó utilizando la herramienta Trello, la cual te permite gestionar cualquier tipo de proyecto y tareas, para utilizarla debes acceder al enlace, registrarte de forma gratuita y seguir las recomendaciones dejadas en el tablero de Trello para utilizar este plan de estudio.

Buenos estudios

[Trello](https://trello.com/b/skhFkXGV/ruta-principiante-en-programaci%C3%B3n "Trello").

### **2. Como utilizar el foro**

El foro es un lugar especial e increíble en donde podemos hacer preguntas relacionadas con los cursos, por lo que es importante que antes de comenzar tus estudios de programación, entiendas cómo usar el foro en nuestra plataforma.

Incentivamos que el foro se utilice solo para preguntas. Si no tienes preguntas, puedes emplearlo para ayudar a otros, fortaleciendo así nuestra comunidad Alura Latam.

[Info Foro](https://www.youtube.com/watch?v=ZhXdFO6SxQ4&ab_channel=AluraLatam "Info Foro").

### **3. Aprender Lógica de Programación**

El racionamiento lógico es la base de programación, pero no se limita a eso, usamos la lógica para actividades de nuestro día a día y la mayoría de las veces sin darnos cuenta. Por ejemplo, cuando éramos niños nuestros padres nos decían que si no acabamos nuestras tareas y obligaciones del colegio, no podríamos salir a jugar con nuestros amigos o no podríamos ver la TV. Si analizamos mejor la frase, se trata de una condición lógica que para poder acceder a un beneficio teníamos que cumplir ciertos requisitos.

Otro ejemplo puede ser a la hora de cepillarnos los dientes, independiente del paso a paso que cada uno siga, un hecho es que no podemos iniciar a cepillarnos sin antes colocar un poco de pasta dental en nuestro cepillo, y también no podemos retirar pasta dental del dentífrico sin antes abrir la tapa del envase. ¿Te das cuenta?, es necesario cumplir una serie de pasos o un algoritmo en el proceso de cepillarse los dientes, solo que hacemos esto en modo automático sin pensarlo.

La lógica de programación, no es más que una secuencia de pasos o instrucciones para resolver un problema. Quien va a resolver el problema, en este caso, es el computador, basado en las instrucciones que el programador pasa o define para la máquina. Por lo tanto, necesitamos entender cuáles son los tipos de instrucciones que el computador entiende y cuál es la mejor forma de transmitir esos comandos para que podamos establecer una comunicación con la máquina.

[Tus primeros pasos estudiando programación](https://www.youtube.com/watch?v=5K97MfYA1sw&ab_channel=AluraLatam "YouTube").

[5 dudas de quien quiere iniciarse en la carrera de programación](https://www.youtube.com/watch?v=5K97MfYA1sw&ab_channel=AluraLatam "Alura LATAM").

[Logica de programación parte 1 : Primeros pasos](https://app.aluracursos.com/course/logica-de-programacion-primeros-pasos "Logica de programacion 1"). Contenido [aqui](./Docs/logica_de_programacion_parte_1.md).

[La carrera de programacion : ¿Que curso tomar primero?](https://www.aluracursos.com/blog/la-carrera-de-programado-que-curso-tomar-primero).

[Logica de programación parte 2 : Conceptos primordiales](https://app.aluracursos.com/course/logica-de-programacion-conceptos-primordiales "Logica de programacion 2"). Contenido [aqui](./Docs/logica_de_programacion_parte_2.md).

[Logica de programación parte 3 : Juegos animaciones](https://app.aluracursos.com/course/logica-de-programacion-juegos-animaciones "Logica de programacion 3"). Contenido [aqui](./Docs/logica_de_programacion_parte_3.md).

[¿Por que estudiar programacion?](https://www.youtube.com/watch?v=PlaQ913yggM&t=306s&ab_channel=AluraLatam "YouTube").

### **4 Crea tus primeras páginas web**

¡Vamos a sumergirnos! Vamos a utilizar HTML y CSS y crear layouts para una página web estática. Aún no vamos a trabajar con interactividad en nuestra página. Pero no te preocupes , porque el próximo paso será unir todo lo que aprendimos en un proyecto web completo.

[HTML5 y CSS3 parte 1 : Primera pagina web](https://app.aluracursos.com/course/html5-css3-primera-pagina-web "HTML5 y CSS3 parte 1"). Contenido [aqui](./Docs/html5_y_css3_parte_1.md).

[HTML5 y CSS3 parte 2 : Posicionamiento listas navegacion](https://app.aluracursos.com/course/html5-css3-posicionamiento-listas-navegacion "HTML5 y CSS3 parte 2"). Contenido [aqui](./Docs/html5_y_css3_parte_2.md).

[HTML5 y CSS3 parte 3 : Formularios tablas](https://app.aluracursos.com/course/html5-css3-formularios-tablas "HTML5 y CSS3 parte 3"). Contenido [aqui](./Docs/html5_y_css3_parte_3.md).

[HTML5 y CSS3 parte 4 : Avanzando css](https://app.aluracursos.com/course/html5-css3-avanzando-css "HTML5 y CSS3 parte 4"). Contenido [aqui](./Docs/html5_y_css3_parte_4.md).

[Carrera IT : ¿Cuales son los profesionales mas solicitados?](https://www.aluracursos.com/blog/carrera-de-ti-cuales-son-los-profesionales-mas-solicitados "Alura LATAM").

### **5 Ganar experiencia con Git**

Es muy importante prepararse para trabajar en equipo en el área de desarrollo, ya que, nadie trabaja solo. Entonces vamos a aprender sobre el sistema de versionamiento de código más utilizado en el mundo: Github, que nos permite compartir código con otras personas de manera simple y segura.

Realizar un buen trabajo en equipo pasa por tener un control de versión de código, así podemos recuperar, revisar y mejorarlo, cada vez más. Conocer un sistema de control de versión como Git es esencial para mantener la calidad y la productividad del trabajo.

[Git y Github : que son y primeros pasos](https://www.aluracursos.com/blog/git-y-github-que-son-y-primeros-pasos "Alura LATAM").

[Alura + Github](https://www.youtube.com/watch?v=-LmFK6skG7s "YouTube").

[Git y Github : controle y comparta su codigo](https://app.aluracursos.com/course/git-github-control-version "Curso"). Contenido [aqui](./Docs/git_y_github.md).

[Que significa pensar como programador](https://www.youtube.com/watch?v=ov7vA5HFe6w "YouTube").

### **6 Diagnóstico de lo aprendido**

Este paso es obligatorio Cuéntanos sobre tus conocimientos adquiridos hasta el momento. Es un diagnóstico personal y por ello es muy importante que seas sincero con tus respuestas.

[Ruta principiante en programacion](https://grupocaelum.typeform.com/to/GjlN1MIg?typeform-source=www.google.com "Ruta principiante en programacion").

### **7 Encara los Challenges de Principiante en Programación**

**¿Qué son los Alura Challenges?**

Es una forma de implementar el Challenge Based Learning, es decir, aprendizaje basado en desafíos que Apple ayudó a crear. Por medio de estos podrás desafiarte a resolver un problema real, investigando las posibles soluciones dentro y fuera del contenido de la ruta de aprendizaje.

**El Challenge ONE: Principiante en programación**, fue ideado para que coloques en práctica los conocimientos que vas adquiriendo mientras realizas los cursos de la ruta de aprendizaje. Consta de dos desafíos para que estudies como resolverlos y los agregues a tu portafolio de proyectos.

Los desafíos están divididos por **Sprint**:

**- Sprint 01: Construye un encriptador de texto con Javascript**

**- Sprint 02: Crea tu propio juego del ahorcado con Javascript**

Cada Sprint tiene una duración de 4 semanas, este es el tiempo sugerido para la realización de cada desafío y durante este periodo contarás con Live Coding para desarrollar el código en vivo y responder a tus preguntas.

La realización de los Challenges es una forma de demostrar, lo que has aprendido y finalmente, podrás publicar tus proyectos, e ir construyendo tu portafolio.

Todo esto mientras comentas y ayudas en proyectos de otros colegas del programa ONE.

¡Manos a la obra!

[Alura challenges](https://www.aluracursos.com/challenges/oracle-one "Alura LATAM")

- [Challenge Encriptador](/Contenido/Challenge/Codificador/ "Challenge Encriptador").
- [Challenge Ahorcado](/Contenido/Challenge/Ahorcado/ "Challenge Ahorcado").

## **Felicidades, completaste la formación Principiante en Programación G3 - ONE**
